////Scanner class of the java. util package is used to read input data
import java.util.Scanner;

class FibonacciSeries_Activity {
	
	//Stored the entered value in variable
	static int N = 1, M = 2, A = 0;
	static void Fibonacci (int count) {
		
		//Iteration Part
		if (count > 1) {
			System.out.println(M);
			A = M + N;
			M = N;
			N = A ;
			Fibonacci (count - 1);
			}
		}
	public static void main (String [] args) {
		
		//Scanner for user input
		Scanner scanner = new Scanner (System.in);
		
		//Number of output
		int count = 20;
		
		//For Printing M
		System.out.println("Input M: ");
		M = scanner.nextInt();
		
		//For Printing N
		System.out.println("Input N: ");
		N = scanner.nextInt();
		Fibonacci (count - 2);
	}

}
