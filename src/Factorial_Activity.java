//Scanner class of the java. util package is used to read input data
import java.util.Scanner;

//Class name
class Factorial_Activity{
	static int fact(int n)
	   {
	       int output;
	       if(n==1){
	         return 1;
	       }
	       //Recursion function calling itself
	       output = fact(n-1)* n;
	       return output;
	   }
	
   public static void main(String args[]){
	      
	   //Scanner for user input
	      Scanner scanner = new Scanner(System.in);
	      
	      System.out.println("Enter the number:");
	      
	      //Stored the entered value in variable
	      int num = scanner.nextInt();
	      
	      //Called the user defined function fact
	      int factorial = fact(num);
	     
	      System.out.println("Factorial of entered number is: "+factorial);
	   }
}